CREATE DATABASE  IF NOT EXISTS `tcpsdb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `tcpsdb`;
-- MySQL dump 10.13  Distrib 8.0.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: tcpsdb
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `consortiums`
--

DROP TABLE IF EXISTS `consortiums`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `consortiums` (
  `consortium_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `consortium_name` varchar(255) NOT NULL,
  `logo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`consortium_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `consortiums`
--

LOCK TABLES `consortiums` WRITE;
/*!40000 ALTER TABLE `consortiums` DISABLE KEYS */;
INSERT INTO `consortiums` (`consortium_id`, `consortium_name`, `logo`) VALUES (1,'Orlen',''),(2,'British Petroleum',''),(3,'Shell',''),(4,'Luk Oil',''),(7,'Lotos',''),(8,'Other','');
/*!40000 ALTER TABLE `consortiums` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `historic_lpg_prices`
--

DROP TABLE IF EXISTS `historic_lpg_prices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `historic_lpg_prices` (
  `historic_lpg_price_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `station_id` bigint(20) unsigned NOT NULL,
  `lpg_price` float unsigned NOT NULL,
  `insert_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`historic_lpg_price_id`),
  KEY `station_historic_lpg_price_fk` (`station_id`),
  KEY `user_historic_lpg_price_fk` (`user_id`),
  CONSTRAINT `station_historic_lpg_price_fk` FOREIGN KEY (`station_id`) REFERENCES `petrol_stations` (`station_id`),
  CONSTRAINT `user_historic_lpg_price_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `historic_lpg_prices`
--

LOCK TABLES `historic_lpg_prices` WRITE;
/*!40000 ALTER TABLE `historic_lpg_prices` DISABLE KEYS */;
INSERT INTO `historic_lpg_prices` (`historic_lpg_price_id`, `user_id`, `station_id`, `lpg_price`, `insert_date`) VALUES (1,5,9,6.21,'2018-10-30 17:40:40'),(2,5,14,1,'2018-10-30 18:00:05'),(3,2,9,1,'2018-10-31 13:59:04'),(4,2,9,9,'2018-10-31 14:01:14'),(5,2,9,2.5,'2018-10-31 14:01:28'),(6,2,18,3,'2018-10-31 16:11:43'),(7,2,11,4,'2018-10-31 17:23:06'),(8,2,11,3,'2018-10-31 17:23:35'),(9,2,27,9,'2018-10-31 17:24:35'),(10,2,10,5,'2018-10-31 18:05:54'),(11,5,14,3.5,'2018-10-31 20:29:25'),(12,5,9,3.5,'2018-10-31 20:29:48'),(13,5,9,4,'2018-10-31 21:42:04'),(14,2,23,1,'2018-10-31 21:43:24'),(15,2,27,9.99,'2018-11-01 16:56:58'),(16,2,27,0,'2018-11-02 19:49:50'),(17,2,21,0.5,'2018-11-04 22:42:00'),(18,NULL,25,2,'2018-11-05 10:31:39'),(19,NULL,25,2.5,'2018-11-05 11:22:38'),(20,NULL,25,4,'2018-11-05 11:40:24'),(21,NULL,10,4,'2018-11-05 11:48:14'),(22,5,16,0.11,'2018-11-11 12:16:33'),(23,5,9,3.7,'2018-11-14 21:24:47'),(24,5,9,2,'2018-11-14 21:25:18'),(25,5,23,9,'2018-11-14 21:34:21'),(26,5,14,6,'2018-11-20 23:30:29'),(27,5,7,2.2,'2018-12-01 00:14:17'),(28,5,31,6,'2018-12-01 22:47:32'),(30,5,7,1,'2018-12-02 22:09:51'),(31,5,33,2.3,'2018-12-02 22:15:43'),(32,5,33,4,'2018-12-02 22:15:59'),(33,5,33,2,'2018-12-02 22:17:53'),(34,5,33,2.1,'2018-12-02 22:21:13'),(35,5,17,4,'2018-12-09 22:28:09'),(36,5,17,0,'2018-12-09 22:28:19'),(37,5,31,4,'2018-12-10 06:58:10'),(38,5,30,1,'2018-12-10 19:47:40'),(39,5,30,2,'2018-12-10 19:49:05'),(40,5,30,1,'2018-12-10 19:49:11'),(41,5,30,2,'2018-12-10 19:49:19'),(43,NULL,9,1,'2018-12-11 15:53:49'),(44,5,29,3,'2018-12-16 22:13:32'),(45,24,19,2,'2018-12-25 14:51:51');
/*!40000 ALTER TABLE `historic_lpg_prices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `historic_on_prices`
--

DROP TABLE IF EXISTS `historic_on_prices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `historic_on_prices` (
  `historic_on_price_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `station_id` bigint(20) unsigned NOT NULL,
  `on_price` float unsigned NOT NULL,
  `insert_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`historic_on_price_id`),
  KEY `station_historic_on_price_fk` (`station_id`),
  KEY `user_historic_on_price_fk` (`user_id`),
  CONSTRAINT `station_historic_on_price_fk` FOREIGN KEY (`station_id`) REFERENCES `petrol_stations` (`station_id`),
  CONSTRAINT `user_historic_on_price_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `historic_on_prices`
--

LOCK TABLES `historic_on_prices` WRITE;
/*!40000 ALTER TABLE `historic_on_prices` DISABLE KEYS */;
INSERT INTO `historic_on_prices` (`historic_on_price_id`, `user_id`, `station_id`, `on_price`, `insert_date`) VALUES (1,6,9,5.5,'2018-10-29 10:31:17'),(2,5,13,5.12,'2018-10-30 15:41:42'),(3,5,14,5.12,'2018-10-30 17:59:06'),(4,5,15,5.12,'2018-10-30 18:08:56'),(5,2,9,1,'2018-10-31 13:59:04'),(6,2,9,4.1,'2018-10-31 14:00:46'),(7,5,25,5.12,'2018-10-31 14:04:12'),(8,2,18,5.88,'2018-10-31 16:11:43'),(9,2,11,3,'2018-10-31 17:23:06'),(10,2,11,4,'2018-10-31 17:23:35'),(11,2,11,5,'2018-10-31 17:24:18'),(12,2,10,5,'2018-10-31 18:05:54'),(13,2,23,8,'2018-10-31 21:43:24'),(14,2,27,9.99,'2018-11-01 16:56:58'),(15,2,27,0,'2018-11-02 19:49:50'),(16,2,21,6,'2018-11-04 22:42:00'),(17,NULL,25,3,'2018-11-05 11:40:24'),(18,5,24,4.5,'2018-11-10 16:54:38'),(19,5,9,3.9,'2018-11-14 21:24:47'),(20,5,9,5,'2018-11-14 21:25:18'),(21,5,14,6,'2018-11-20 23:30:29'),(23,5,7,0,'2018-12-02 22:09:50'),(24,5,33,5.4,'2018-12-02 22:15:43'),(25,5,33,3,'2018-12-02 22:15:59'),(26,5,33,5.2,'2018-12-02 22:17:53'),(27,5,33,5.1,'2018-12-02 22:21:13'),(28,5,17,3,'2018-12-09 22:28:09'),(29,5,9,9,'2018-12-09 22:50:07'),(30,5,9,3,'2018-12-09 22:50:18'),(31,5,9,3,'2018-12-09 22:50:29'),(32,5,31,3,'2018-12-10 06:50:52'),(33,5,30,6,'2018-12-10 19:47:52'),(34,5,30,0,'2018-12-10 19:48:05'),(35,5,30,4,'2018-12-10 19:48:35'),(37,NULL,9,4.8,'2018-12-11 15:53:49'),(38,5,21,5.88,'2018-12-11 21:54:14'),(39,5,29,3,'2018-12-16 22:13:32'),(40,24,19,4,'2018-12-25 14:51:51');
/*!40000 ALTER TABLE `historic_on_prices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `historic_pb95_prices`
--

DROP TABLE IF EXISTS `historic_pb95_prices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `historic_pb95_prices` (
  `historic_pb95_price_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `station_id` bigint(20) unsigned NOT NULL,
  `pb95_price` float unsigned NOT NULL,
  `insert_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`historic_pb95_price_id`),
  KEY `station_historic_pb95_price_fk` (`station_id`),
  KEY `user_historic_pb95_price_fk` (`user_id`),
  CONSTRAINT `station_historic_pb95_price_fk` FOREIGN KEY (`station_id`) REFERENCES `petrol_stations` (`station_id`),
  CONSTRAINT `user_historic_pb95_price_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `historic_pb95_prices`
--

LOCK TABLES `historic_pb95_prices` WRITE;
/*!40000 ALTER TABLE `historic_pb95_prices` DISABLE KEYS */;
INSERT INTO `historic_pb95_prices` (`historic_pb95_price_id`, `user_id`, `station_id`, `pb95_price`, `insert_date`) VALUES (1,3,9,5.15,'2018-10-29 10:25:17'),(2,3,9,5.45,'2018-10-29 10:30:17'),(3,5,9,2,'2018-10-30 17:40:39'),(4,5,14,5.14,'2018-10-30 18:00:05'),(5,5,15,1,'2018-10-30 18:08:56'),(6,2,9,5,'2018-10-31 13:58:04'),(7,2,9,1,'2018-10-31 13:59:04'),(8,2,9,5,'2018-10-31 14:00:18'),(9,2,9,5,'2018-10-31 14:00:31'),(10,2,24,5,'2018-10-31 14:23:53'),(11,2,18,7.98,'2018-10-31 16:11:43'),(12,2,11,1,'2018-10-31 17:23:06'),(13,2,11,6,'2018-10-31 17:23:35'),(14,2,10,5,'2018-10-31 18:05:54'),(15,2,23,3,'2018-10-31 21:43:24'),(16,2,10,2,'2018-11-01 16:56:32'),(17,2,27,9.99,'2018-11-01 16:56:58'),(18,2,27,0,'2018-11-02 19:49:50'),(19,2,21,1,'2018-11-04 22:42:00'),(20,NULL,25,1,'2018-11-05 11:40:24'),(21,NULL,10,1,'2018-11-05 11:48:14'),(22,5,9,3,'2018-11-14 21:24:47'),(23,5,9,5,'2018-11-14 21:25:18'),(24,5,23,4,'2018-11-14 21:34:21'),(25,5,14,6,'2018-11-20 23:30:29'),(27,5,7,4.65,'2018-12-02 22:09:50'),(28,5,33,4.6,'2018-12-02 22:15:43'),(29,5,33,1,'2018-12-02 22:15:59'),(30,5,33,4.95,'2018-12-02 22:17:53'),(31,5,33,4.95,'2018-12-02 22:21:13'),(32,5,33,6,'2018-12-02 22:21:29'),(33,5,33,4.8,'2018-12-02 22:21:44'),(34,5,17,1,'2018-12-09 22:28:09'),(35,5,17,0,'2018-12-09 22:30:08'),(36,5,31,1,'2018-12-10 06:50:52'),(37,5,31,2,'2018-12-10 19:50:46'),(39,NULL,9,4.5,'2018-12-11 15:53:49'),(40,5,29,3,'2018-12-16 22:13:32'),(41,24,19,5,'2018-12-25 14:51:51'),(42,5,16,1,'2018-12-27 17:08:14');
/*!40000 ALTER TABLE `historic_pb95_prices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `historic_pb98_prices`
--

DROP TABLE IF EXISTS `historic_pb98_prices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `historic_pb98_prices` (
  `historic_pb98_price_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `station_id` bigint(20) unsigned NOT NULL,
  `pb98_price` float unsigned NOT NULL,
  `insert_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`historic_pb98_price_id`),
  KEY `station_historic_pb98_price_fk` (`station_id`),
  KEY `user_historic_pb98_price_fk` (`user_id`),
  CONSTRAINT `station_historic_pb98_price_fk` FOREIGN KEY (`station_id`) REFERENCES `petrol_stations` (`station_id`),
  CONSTRAINT `user_historic_pb98_price_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `historic_pb98_prices`
--

LOCK TABLES `historic_pb98_prices` WRITE;
/*!40000 ALTER TABLE `historic_pb98_prices` DISABLE KEYS */;
INSERT INTO `historic_pb98_prices` (`historic_pb98_price_id`, `user_id`, `station_id`, `pb98_price`, `insert_date`) VALUES (1,6,9,2,'2018-09-29 10:30:17'),(2,6,9,3,'2018-10-29 10:30:17'),(3,6,9,4,'2018-10-29 10:30:17'),(4,2,9,1,'2018-10-31 13:59:04'),(5,2,9,5.63,'2018-10-31 14:01:28'),(6,2,18,5.54,'2018-10-31 16:11:43'),(7,2,9,2,'2018-10-31 16:15:23'),(8,2,11,2,'2018-10-31 17:23:06'),(9,2,11,5,'2018-10-31 17:23:35'),(10,2,10,5,'2018-10-31 18:05:54'),(11,2,23,4,'2018-10-31 21:43:24'),(12,2,27,9.99,'2018-11-01 16:56:58'),(13,2,27,0,'2018-11-02 19:49:50'),(14,2,21,4,'2018-11-04 22:42:00'),(15,NULL,25,2,'2018-11-05 11:40:24'),(16,5,9,3,'2018-11-14 21:24:47'),(17,5,9,5,'2018-11-14 21:25:18'),(18,5,14,5,'2018-11-14 21:34:45'),(19,5,14,6,'2018-11-20 23:30:29'),(20,5,10,8,'2018-11-27 15:15:18'),(21,5,31,8,'2018-12-01 22:47:32'),(23,5,7,5,'2018-12-02 22:09:50'),(24,5,33,6.7,'2018-12-02 22:15:43'),(25,5,33,2,'2018-12-02 22:15:59'),(26,5,33,0,'2018-12-02 22:17:53'),(27,5,33,9,'2018-12-02 22:18:21'),(28,5,33,9,'2018-12-02 22:20:23'),(29,5,33,9,'2018-12-02 22:20:37'),(30,5,33,9,'2018-12-02 22:20:44'),(31,5,33,0,'2018-12-02 22:21:13'),(32,5,17,2,'2018-12-09 22:28:09'),(33,5,9,0,'2018-12-09 22:30:57'),(34,5,9,8,'2018-12-09 22:31:59'),(35,5,9,2,'2018-12-09 22:32:05'),(36,5,31,1,'2018-12-09 22:58:45'),(37,5,31,2,'2018-12-10 06:57:50'),(38,5,30,5,'2018-12-10 19:47:52'),(40,NULL,9,6,'2018-12-11 15:53:49'),(41,NULL,9,4.7,'2018-12-11 15:54:05'),(42,5,29,3,'2018-12-16 22:13:32'),(43,24,19,6,'2018-12-25 14:51:51');
/*!40000 ALTER TABLE `historic_pb98_prices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `historic_prices`
--

DROP TABLE IF EXISTS `historic_prices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `historic_prices` (
  `historic_price_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `station_id` bigint(20) unsigned NOT NULL,
  `pb95_price` float unsigned DEFAULT NULL,
  `pb98_price` float unsigned DEFAULT NULL,
  `on_price` float unsigned DEFAULT NULL,
  `lpg_price` float unsigned DEFAULT NULL,
  `insert_date` datetime NOT NULL,
  PRIMARY KEY (`historic_price_id`),
  KEY `station_historicprices_fk` (`station_id`),
  KEY `user_historicprices_fk` (`user_id`),
  CONSTRAINT `station_historicprices_fk` FOREIGN KEY (`station_id`) REFERENCES `petrol_stations` (`station_id`),
  CONSTRAINT `user_historicprices_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `historic_prices`
--

LOCK TABLES `historic_prices` WRITE;
/*!40000 ALTER TABLE `historic_prices` DISABLE KEYS */;
INSERT INTO `historic_prices` (`historic_price_id`, `user_id`, `station_id`, `pb95_price`, `pb98_price`, `on_price`, `lpg_price`, `insert_date`) VALUES (1,2,7,5.41,5.6,5.3,1.45,'2018-10-18 10:25:17'),(2,2,7,5.41,5.6,5.3,1.45,'2018-09-18 10:25:17'),(3,2,7,5.46,5.3,5.1,2.45,'2018-10-10 10:25:17');
/*!40000 ALTER TABLE `historic_prices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_access_token`
--

DROP TABLE IF EXISTS `oauth_access_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `oauth_access_token` (
  `token_id` varchar(256) DEFAULT NULL,
  `token` blob,
  `authentication_id` varchar(256) NOT NULL,
  `user_name` varchar(256) DEFAULT NULL,
  `client_id` varchar(256) DEFAULT NULL,
  `authentication` blob,
  `refresh_token` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`authentication_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_token`
--

LOCK TABLES `oauth_access_token` WRITE;
/*!40000 ALTER TABLE `oauth_access_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_access_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_token`
--

DROP TABLE IF EXISTS `oauth_refresh_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `oauth_refresh_token` (
  `token_id` varchar(256) DEFAULT NULL,
  `token` blob,
  `authentication` blob
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_token`
--

LOCK TABLES `oauth_refresh_token` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `petrol_prices`
--

DROP TABLE IF EXISTS `petrol_prices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `petrol_prices` (
  `price_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `station_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `pb95_price` float unsigned NOT NULL,
  `pb98_price` float unsigned NOT NULL,
  `on_price` float unsigned NOT NULL,
  `lpg_price` float unsigned NOT NULL,
  `insert_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`price_id`),
  UNIQUE KEY `station_id_UNIQUE` (`station_id`),
  KEY `station_prices_fk` (`station_id`),
  KEY `user_petrolprices_fk` (`user_id`),
  CONSTRAINT `station_prices_fk` FOREIGN KEY (`station_id`) REFERENCES `petrol_stations` (`station_id`),
  CONSTRAINT `user_petrolprices_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `petrol_prices`
--

LOCK TABLES `petrol_prices` WRITE;
/*!40000 ALTER TABLE `petrol_prices` DISABLE KEYS */;
INSERT INTO `petrol_prices` (`price_id`, `station_id`, `user_id`, `pb95_price`, `pb98_price`, `on_price`, `lpg_price`, `insert_date`) VALUES (1,12,2,5,5.41,5.17,2.36,'2018-10-17 10:47:20'),(3,7,5,4.65,5,0,1,'2018-12-02 22:09:50'),(4,9,NULL,4.5,4.7,4.8,1,'2018-12-11 15:54:05'),(5,8,5,5.11,5.45,4.56,2.4,'2018-10-18 12:54:29'),(7,10,5,1,8,5,4,'2018-11-27 15:15:18'),(9,13,5,0,0,5.12,0,'2018-10-30 15:41:42'),(10,14,5,6,6,6,6,'2018-11-20 23:30:29'),(11,15,5,1,0,5.12,0,'2018-10-30 18:08:55'),(12,25,NULL,1,2,3,4,'2018-11-05 11:40:24'),(13,24,5,5,0,4.5,0,'2018-11-10 16:54:38'),(14,18,2,7.98,5.54,5.88,3,'2018-10-31 16:11:43'),(15,11,2,6,5,5,3,'2018-10-31 17:24:18'),(16,27,2,0,0,0,0,'2018-11-02 19:49:50'),(17,23,5,4,4,8,9,'2018-11-14 21:34:21'),(18,21,5,1,4,5.88,0.5,'2018-12-11 21:54:14'),(19,16,5,1,0,0,0.11,'2018-12-27 17:08:13'),(20,31,5,2,2,3,4,'2018-12-10 19:50:46'),(22,33,5,4.8,0,5.1,2.1,'2018-12-02 22:21:44'),(23,17,5,0,2,3,0,'2018-12-09 22:30:08'),(24,30,5,0,5,4,2,'2018-12-10 19:49:18'),(25,29,5,3,3,3,3,'2018-12-16 22:13:32'),(26,19,24,5,6,4,2,'2018-12-25 14:51:51');
/*!40000 ALTER TABLE `petrol_prices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `petrol_stations`
--

DROP TABLE IF EXISTS `petrol_stations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `petrol_stations` (
  `station_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `consortium_id` bigint(20) unsigned NOT NULL,
  `longitude` double NOT NULL,
  `latitude` double NOT NULL,
  `has_food` tinyint(1) NOT NULL,
  `apartment_number` varchar(20) NOT NULL,
  `postal_code` char(6) DEFAULT NULL,
  `station_name` varchar(100) NOT NULL,
  `city` varchar(50) NOT NULL,
  `street` varchar(70) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`station_id`),
  KEY `consortium_station_fk` (`consortium_id`),
  CONSTRAINT `consortium_station_fk` FOREIGN KEY (`consortium_id`) REFERENCES `consortiums` (`consortium_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `petrol_stations`
--

LOCK TABLES `petrol_stations` WRITE;
/*!40000 ALTER TABLE `petrol_stations` DISABLE KEYS */;
INSERT INTO `petrol_stations` (`station_id`, `consortium_id`, `longitude`, `latitude`, `has_food`, `apartment_number`, `postal_code`, `station_name`, `city`, `street`, `description`) VALUES (1,1,29.5142,50.5452,1,'22','40-219','Orlen na Murckowskiej','Katowice','Murckowska','stacja jaka jest każdy widzi'),(2,1,29.5142,50.5452,1,'22','40-219','Orlen na Murckowskiej2','Katowice2','Murckowska','stacja jaka jest każdy widzi'),(3,1,29.5142,50.5452,1,'222','40-219','Orlen na Murckowskiej3','Katowice','Murckowska','stacja jaka jest każdy widzi'),(4,1,29.5142,50.5452,1,'222','40-219','Orlen na Murckowskiej4','Katowice','Murckowska4','stacja jaka jest każdy widzi'),(5,1,29.5142,50.5452,1,'222','40-219','Orlen na Murckowskiej5','Katowice','Murckowska5','stacja jaka jest każdy widzi'),(6,1,29.5142,50.5452,1,'222','40-219','Orlen na Murckowskiej6','Katowice','Murckowska6','stacja jaka jest każdy widzi'),(7,1,18.8810349,50.2513487,1,'11','40-219','Shell na A4','Katowice','Piastów Śląskich','stacja jaka jest każdy widzi'),(8,4,-95.96767899999999,35.450502,0,'7','j','G','H','G','G'),(9,2,19.0622031,50.2686061,1,'200','bv','qqq','Katowice','Rozdzienskiego','vcb'),(10,3,19.060035,50.2684254,1,'198','4','fre','Katowice','Rozdzienskiego','fdg'),(11,3,19.049131,50.268057,1,'10','1','kol','Katowice','Lubuska','fdf'),(12,1,18.9849877,50.2725837,1,'20','40-219','Shell na A42','Katowice','Piastów Śląskich','stacja jaka jest każdy widzi'),(13,1,18.9849916,50.272211,1,'21','40-219','Shell na A43','Katowice','Piastów Śląskich','stacja jaka jest każdy widzi'),(14,3,19.049193,50.268898,1,'9','40-219','qaz','Katowice','Lubuska','jhh'),(15,3,19.0489468,50.2681909,1,'8','55','asdcd','Katowice','Lubuska','tretg'),(16,3,19.049218,50.268076,1,'12','54514','efd','Katowice','Lubuska','frjknkjdnkej'),(17,3,19.047606,50.269521,1,'2a','jfhf','Ggmj','Katowice','Kurpiowska 2','Hdhfidjx'),(18,1,19.1074315,50.1828249,1,'2lk','40-219','Shell na A44','Katowice','Piastów Śląskich','stacja jaka jest każdy widzi'),(19,3,18.913186,50.2497469,1,'2lka','40-219','Shell na A45','Katowice','Piastów Śląskich','stacja jaka jest każdy widzi'),(20,3,18.9985911,50.2479816,1,'90','niwim','Fajna Stacja Koło AWF','KATOWICE ','Mikołowska','Stacja jest obok przystanku KZKGOP'),(21,4,18.999897,50.248331,1,'100','costam','COIG','Katowice','Mikołowska','Niskie płace :('),(22,4,18.681629,50.283564,0,'111','40-100','Artus','Gliwice','Pszyczynska','Marna stacja koło LidlaLidlal'),(23,3,18.992203,50.2350106,0,'1','g dm','Hdhieyhdidghdj Fud Rudi jsbid D hotdog euuduhej','Katowice','Friedricha Wilhelma Grundmanna','Uf+djirj fjfbjdjdbeid jidjn. Djdjdjjdjjdibdhfihhdhdbdhdhk'),(24,1,18.6727941,50.2873157,1,'45','44-100','Orlen na skrzyżowaniu Przyńskiej i Jasnej','Gliwice','Pszczyńska','stacja jaka jest każdy widzi'),(25,1,18.6981593,50.2753524,1,'44','44-100','Orlen na skrzyżowaniu Przyńskiej i Jasnej v2','Gliwice','Pszczyńska','stacja jaka jest każdy widzi'),(26,1,18.672761,50.2873327,1,'43','44-100','Orlen na skrzyżowaniu Przyńskiej i Jasnej v3','Gliwice','Pszczyńska','stacja jaka jest każdy widzi'),(27,1,18.6708929,50.2873325,1,'42','44-100','Orlen na skrzyżowaniu Przyńskiej i Jasnej v4','Gliwice','Pszczyńska','stacja jaka jest każdy widzi'),(28,1,19.048059,50.2689973,0,'12','hfhf','Hfkfjdjfjfjf','Katowice','Kurpiowska','Jtjfjfbfjf c f jfjfbfke fhrndjdhdhwiwo38eur fj'),(29,4,19.014311,50.259963,1,'1','56','Vyfhy','Katowice','Plac Wolnosci','Hxhrjcheufhfif fjfijrbdiehfk4vf tirhfirjhfurjfhr fjfheihdhfhrhehdhf fjdhdhrufhrhfhhfhfjdhdhrufhrhfhhfh, '),(30,3,19.0120826,50.2562211,1,'15','40-354','Jakaś Sobie Stacja','Katowice','Mikołowska','Ythtfhugbjjh hfjdjdhjd didbdjfjfod'),(31,4,19.0485839,50.26745289999999,1,'3','40-220','Stacja 1','Katowice','Kujawska','Ydufhfjr fhdufjdjd fjdjfjrhehf fj'),(33,4,18.6789227,50.2840843,1,'100A','44-100','Luk Oil Gliwice 4','Gliwice','Pszczyńska','Stacja naprzeciwko Lidla'),(34,3,19.0097949,50.25444270000001,1,'23A','40-074','Stacja2','Katowice','Mikołowaka','Koło pałacu młodzieży ');
/*!40000 ALTER TABLE `petrol_stations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ratings`
--

DROP TABLE IF EXISTS `ratings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ratings` (
  `rating_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `station_id` bigint(20) unsigned NOT NULL,
  `rate` double unsigned DEFAULT NULL,
  PRIMARY KEY (`rating_id`),
  KEY `user_rating_fk` (`user_id`),
  KEY `station_rating_fk` (`station_id`),
  CONSTRAINT `station_rating_fk` FOREIGN KEY (`station_id`) REFERENCES `petrol_stations` (`station_id`),
  CONSTRAINT `user_rating_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ratings`
--

LOCK TABLES `ratings` WRITE;
/*!40000 ALTER TABLE `ratings` DISABLE KEYS */;
INSERT INTO `ratings` (`rating_id`, `user_id`, `station_id`, `rate`) VALUES (1,2,9,5),(2,5,9,5),(3,6,9,2),(7,8,12,4),(9,8,23,1.25),(10,5,22,5),(11,5,23,5),(12,2,22,1),(13,2,21,1),(14,2,18,3),(15,2,10,3),(16,2,11,3),(17,2,7,1),(18,2,23,1),(19,2,29,4),(20,2,16,4),(25,5,30,4),(26,5,14,5),(27,5,24,3),(28,5,25,2),(29,5,16,1),(30,5,21,2),(31,5,19,4),(32,5,13,3),(33,5,28,3),(34,5,7,4),(35,5,15,4),(36,5,10,1),(37,5,11,3),(38,5,33,3),(39,5,31,1),(40,6,33,2),(42,24,28,2),(43,24,14,3);
/*!40000 ALTER TABLE `ratings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users` (
  `user_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` char(60) NOT NULL,
  `user_role` varchar(25) NOT NULL,
  `enabled` tinyint(3) unsigned NOT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `email` varchar(45) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`user_id`, `username`, `password`, `user_role`, `enabled`, `first_name`, `last_name`, `email`) VALUES (2,'user1','$2a$10$ScagGVj7s6k51zr4kCmgx.op.xGTQD8xSr.3k9D11oWIiAuBDDbBS','android_user',1,'User1','Userski1','user1@email.com'),(3,'user2','$2a$04$X/hydFtnhv6XKK95V6q4qOwLlvjWpHfCXS3NLUu.8FkULJv44Fuvy','android_user',1,'User2','Userski2','user2@email.com'),(4,'registration','$2a$04$Q.JPzRXvi8nOCnlc34s.duwuTaLyD06bEUV6WImqoGeWLddii7wKy','registration_user',1,NULL,NULL,''),(5,'test1','$2a$04$4e9eybJBL/LmD2JFKhfbfOpEf9lv0EaGYQ1SPNp93q6A0lPoXg14S','android_user',1,'Test','Testowski','test@email.com'),(6,'test2','$2a$04$G0J2.nHDysKENwyFsB176OXWnpoJVGp759/cCUaegJqP.pQWW7I8W','android_user',1,'Test2','Testowski2','test2@email.com'),(8,'test3','$2a$04$78pkLg0.u9RUVD0iJidLW.N2bZE1ys096CTbN6aBfloh4prrH6l0i','android_user',1,'Test3','Testowski3','test3@email.com'),(14,'gks','$2a$04$xQxHiALHDJTU2A3CDaR80.dQdqiSHkHvBdMVyHwzrkVQcidEcSt26','android_user',1,'Gks','Gieksa','grzegorz2200@gmail.com'),(15,'grzegorz863','$2a$04$YycdrDulF2Pvt3uHPe3pouQg8hnF8nz2LieJZV4fG9PR7hiY3XJxS','android_user',1,'Grzes','Nowak','grzegorz2200@gmail.com'),(16,'test9','$2a$04$Bk4ErewPViUqtHg4RvYMKuumlDBGkeNFDuemyI6JK6mbKHZifVIJO','android_user',1,'G','G','grzegorz2200@gmail.com'),(17,'gh','$2a$04$jllRdcYtZS6IF5RTGQNkoeKdQACWXb0VTDUlUHZilRYwyUTNsdim6','android_user',1,'Grzegorz','Nowak','grzegorz2200@gmail.com'),(18,'test4','$2a$04$aDMNYkOI.2pONJpxraLOfuW8vaACrAEC2u.1km8R6O6bbDjCo.epS','android_user',1,'Test4','Testowski4','test4@email.com'),(24,'kasia1','$2a$04$5.HTtC0Cf.c7R0Xl.UCj4OrplhXho7Qja9J8nYDLRHfTBDw4Wu/I2','android_user',1,'Kasia','Nowak','grzegorz2200@gmail.com');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'tcpsdb'
--
/*!50003 DROP PROCEDURE IF EXISTS `delete_petrol_station` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`admin`@`%` PROCEDURE `delete_petrol_station`(p_station_id bigint)
BEGIN
	delete from historic_pb95_prices where station_id = p_station_id;
DELETE FROM historic_pb98_prices 
WHERE
    station_id = p_station_id;
DELETE FROM historic_on_prices 
WHERE
    station_id = p_station_id;
DELETE FROM historic_lpg_prices 
WHERE
    station_id = p_station_id;
DELETE FROM petrol_prices 
WHERE
    station_id = p_station_id;
DELETE FROM ratings 
WHERE
    station_id = p_station_id;
DELETE FROM petrol_stations 
WHERE
    station_id = p_station_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

CREATE USER 'hibernate_user'@'localhost' IDENTIFIED BY 'qN;8EpnM=VcHeY//#(:3';
GRANT select, insert, update, delete ON tcpsdb.* TO 'hibernate_user'@'localhost';

-- Dump completed on 2019-01-05 20:34:24
